/*
 * This file was generated with xdt-gen-visibility.  Do not edit it directly.
 */

#include <glib.h>

#if defined(ENABLE_SYMBOL_VISIBILITY)     && defined(__GNUC__)     && defined(G_HAVE_GNUC_VISIBILITY)

G_GNUC_BEGIN_IGNORE_DEPRECATIONS

#ifdef __LIBXFCE4PANEL_CONFIG_H__

extern __typeof(libxfce4panel_check_version) IA__libxfce4panel_check_version __attribute__((visibility("hidden")));
#define libxfce4panel_check_version IA__libxfce4panel_check_version

extern __typeof(libxfce4panel_major_version) IA__libxfce4panel_major_version __attribute__((visibility("hidden")));
#define libxfce4panel_major_version IA__libxfce4panel_major_version

extern __typeof(libxfce4panel_micro_version) IA__libxfce4panel_micro_version __attribute__((visibility("hidden")));
#define libxfce4panel_micro_version IA__libxfce4panel_micro_version

extern __typeof(libxfce4panel_minor_version) IA__libxfce4panel_minor_version __attribute__((visibility("hidden")));
#define libxfce4panel_minor_version IA__libxfce4panel_minor_version

#endif /* __LIBXFCE4PANEL_CONFIG_H__ */


#ifdef __LIBXFCE4PANEL_ENUM_TYPES_H__

extern __typeof(xfce_screen_position_get_type) IA__xfce_screen_position_get_type __attribute__((visibility("hidden"))) G_GNUC_CONST;
#define xfce_screen_position_get_type IA__xfce_screen_position_get_type

extern __typeof(xfce_panel_plugin_mode_get_type) IA__xfce_panel_plugin_mode_get_type __attribute__((visibility("hidden"))) G_GNUC_CONST;
#define xfce_panel_plugin_mode_get_type IA__xfce_panel_plugin_mode_get_type

#endif /* __LIBXFCE4PANEL_ENUM_TYPES_H__ */


#ifdef __XFCE_ARROW_BUTTON_H__

extern __typeof(xfce_arrow_button_get_type) IA__xfce_arrow_button_get_type __attribute__((visibility("hidden"))) G_GNUC_CONST;
#define xfce_arrow_button_get_type IA__xfce_arrow_button_get_type

extern __typeof(xfce_arrow_button_new) IA__xfce_arrow_button_new __attribute__((visibility("hidden"))) G_GNUC_MALLOC G_GNUC_WARN_UNUSED_RESULT;
#define xfce_arrow_button_new IA__xfce_arrow_button_new

extern __typeof(xfce_arrow_button_get_arrow_type) IA__xfce_arrow_button_get_arrow_type __attribute__((visibility("hidden")));
#define xfce_arrow_button_get_arrow_type IA__xfce_arrow_button_get_arrow_type

extern __typeof(xfce_arrow_button_set_arrow_type) IA__xfce_arrow_button_set_arrow_type __attribute__((visibility("hidden")));
#define xfce_arrow_button_set_arrow_type IA__xfce_arrow_button_set_arrow_type

extern __typeof(xfce_arrow_button_get_blinking) IA__xfce_arrow_button_get_blinking __attribute__((visibility("hidden")));
#define xfce_arrow_button_get_blinking IA__xfce_arrow_button_get_blinking

extern __typeof(xfce_arrow_button_set_blinking) IA__xfce_arrow_button_set_blinking __attribute__((visibility("hidden")));
#define xfce_arrow_button_set_blinking IA__xfce_arrow_button_set_blinking

#endif /* __XFCE_ARROW_BUTTON_H__ */


#ifdef __XFCE_PANEL_CONVENIENCE_H__

extern __typeof(xfce_panel_create_button) IA__xfce_panel_create_button __attribute__((visibility("hidden"))) G_GNUC_MALLOC G_GNUC_WARN_UNUSED_RESULT;
#define xfce_panel_create_button IA__xfce_panel_create_button

extern __typeof(xfce_panel_create_toggle_button) IA__xfce_panel_create_toggle_button __attribute__((visibility("hidden"))) G_GNUC_MALLOC G_GNUC_WARN_UNUSED_RESULT;
#define xfce_panel_create_toggle_button IA__xfce_panel_create_toggle_button

extern __typeof(xfce_panel_get_channel_name) IA__xfce_panel_get_channel_name __attribute__((visibility("hidden")));
#define xfce_panel_get_channel_name IA__xfce_panel_get_channel_name

extern __typeof(xfce_panel_pixbuf_from_source) IA__xfce_panel_pixbuf_from_source __attribute__((visibility("hidden")));
#define xfce_panel_pixbuf_from_source IA__xfce_panel_pixbuf_from_source

extern __typeof(xfce_panel_pixbuf_from_source_at_size) IA__xfce_panel_pixbuf_from_source_at_size __attribute__((visibility("hidden")));
#define xfce_panel_pixbuf_from_source_at_size IA__xfce_panel_pixbuf_from_source_at_size

extern __typeof(xfce_panel_set_image_from_source) IA__xfce_panel_set_image_from_source __attribute__((visibility("hidden")));
#define xfce_panel_set_image_from_source IA__xfce_panel_set_image_from_source

#endif /* __XFCE_PANEL_CONVENIENCE_H__ */


#ifdef __XFCE_PANEL_IMAGE_H__

extern __typeof(xfce_panel_image_get_type) IA__xfce_panel_image_get_type __attribute__((visibility("hidden"))) G_GNUC_CONST;
#define xfce_panel_image_get_type IA__xfce_panel_image_get_type

extern __typeof(xfce_panel_image_new) IA__xfce_panel_image_new __attribute__((visibility("hidden"))) G_GNUC_MALLOC;
#define xfce_panel_image_new IA__xfce_panel_image_new

extern __typeof(xfce_panel_image_new_from_pixbuf) IA__xfce_panel_image_new_from_pixbuf __attribute__((visibility("hidden"))) G_GNUC_MALLOC;
#define xfce_panel_image_new_from_pixbuf IA__xfce_panel_image_new_from_pixbuf

extern __typeof(xfce_panel_image_new_from_source) IA__xfce_panel_image_new_from_source __attribute__((visibility("hidden"))) G_GNUC_MALLOC;
#define xfce_panel_image_new_from_source IA__xfce_panel_image_new_from_source

extern __typeof(xfce_panel_image_set_from_pixbuf) IA__xfce_panel_image_set_from_pixbuf __attribute__((visibility("hidden")));
#define xfce_panel_image_set_from_pixbuf IA__xfce_panel_image_set_from_pixbuf

extern __typeof(xfce_panel_image_set_from_source) IA__xfce_panel_image_set_from_source __attribute__((visibility("hidden")));
#define xfce_panel_image_set_from_source IA__xfce_panel_image_set_from_source

extern __typeof(xfce_panel_image_set_size) IA__xfce_panel_image_set_size __attribute__((visibility("hidden")));
#define xfce_panel_image_set_size IA__xfce_panel_image_set_size

extern __typeof(xfce_panel_image_get_size) IA__xfce_panel_image_get_size __attribute__((visibility("hidden")));
#define xfce_panel_image_get_size IA__xfce_panel_image_get_size

extern __typeof(xfce_panel_image_clear) IA__xfce_panel_image_clear __attribute__((visibility("hidden")));
#define xfce_panel_image_clear IA__xfce_panel_image_clear

#endif /* __XFCE_PANEL_IMAGE_H__ */


#ifdef __XFCE_PANEL_PLUGIN_H__

extern __typeof(xfce_panel_plugin_get_type) IA__xfce_panel_plugin_get_type __attribute__((visibility("hidden"))) G_GNUC_CONST;
#define xfce_panel_plugin_get_type IA__xfce_panel_plugin_get_type

extern __typeof(xfce_panel_plugin_get_name) IA__xfce_panel_plugin_get_name __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_name IA__xfce_panel_plugin_get_name

extern __typeof(xfce_panel_plugin_get_display_name) IA__xfce_panel_plugin_get_display_name __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_display_name IA__xfce_panel_plugin_get_display_name

extern __typeof(xfce_panel_plugin_get_comment) IA__xfce_panel_plugin_get_comment __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_comment IA__xfce_panel_plugin_get_comment

extern __typeof(xfce_panel_plugin_get_unique_id) IA__xfce_panel_plugin_get_unique_id __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_unique_id IA__xfce_panel_plugin_get_unique_id

extern __typeof(xfce_panel_plugin_get_property_base) IA__xfce_panel_plugin_get_property_base __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_property_base IA__xfce_panel_plugin_get_property_base

extern __typeof(xfce_panel_plugin_get_arguments) IA__xfce_panel_plugin_get_arguments __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_arguments IA__xfce_panel_plugin_get_arguments

extern __typeof(xfce_panel_plugin_get_size) IA__xfce_panel_plugin_get_size __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_size IA__xfce_panel_plugin_get_size

extern __typeof(xfce_panel_plugin_get_small) IA__xfce_panel_plugin_get_small __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_small IA__xfce_panel_plugin_get_small

extern __typeof(xfce_panel_plugin_get_icon_size) IA__xfce_panel_plugin_get_icon_size __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_icon_size IA__xfce_panel_plugin_get_icon_size

extern __typeof(xfce_panel_plugin_get_expand) IA__xfce_panel_plugin_get_expand __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_expand IA__xfce_panel_plugin_get_expand

extern __typeof(xfce_panel_plugin_set_expand) IA__xfce_panel_plugin_set_expand __attribute__((visibility("hidden")));
#define xfce_panel_plugin_set_expand IA__xfce_panel_plugin_set_expand

extern __typeof(xfce_panel_plugin_get_shrink) IA__xfce_panel_plugin_get_shrink __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_shrink IA__xfce_panel_plugin_get_shrink

extern __typeof(xfce_panel_plugin_set_shrink) IA__xfce_panel_plugin_set_shrink __attribute__((visibility("hidden")));
#define xfce_panel_plugin_set_shrink IA__xfce_panel_plugin_set_shrink

extern __typeof(xfce_panel_plugin_set_small) IA__xfce_panel_plugin_set_small __attribute__((visibility("hidden")));
#define xfce_panel_plugin_set_small IA__xfce_panel_plugin_set_small

extern __typeof(xfce_panel_plugin_get_locked) IA__xfce_panel_plugin_get_locked __attribute__((visibility("hidden")));
#define xfce_panel_plugin_get_locked IA__xfce_panel_plugin_get_locked

extern __typeof(xfce_panel_plugin_get_orientation) IA__xfce_panel_plugin_get_orientation __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_orientation IA__xfce_panel_plugin_get_orientation

extern __typeof(xfce_panel_plugin_get_mode) IA__xfce_panel_plugin_get_mode __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_mode IA__xfce_panel_plugin_get_mode

extern __typeof(xfce_panel_plugin_get_nrows) IA__xfce_panel_plugin_get_nrows __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_nrows IA__xfce_panel_plugin_get_nrows

extern __typeof(xfce_panel_plugin_get_screen_position) IA__xfce_panel_plugin_get_screen_position __attribute__((visibility("hidden"))) G_GNUC_PURE;
#define xfce_panel_plugin_get_screen_position IA__xfce_panel_plugin_get_screen_position

extern __typeof(xfce_panel_plugin_take_window) IA__xfce_panel_plugin_take_window __attribute__((visibility("hidden")));
#define xfce_panel_plugin_take_window IA__xfce_panel_plugin_take_window

extern __typeof(xfce_panel_plugin_add_action_widget) IA__xfce_panel_plugin_add_action_widget __attribute__((visibility("hidden")));
#define xfce_panel_plugin_add_action_widget IA__xfce_panel_plugin_add_action_widget

extern __typeof(xfce_panel_plugin_menu_insert_item) IA__xfce_panel_plugin_menu_insert_item __attribute__((visibility("hidden")));
#define xfce_panel_plugin_menu_insert_item IA__xfce_panel_plugin_menu_insert_item

extern __typeof(xfce_panel_plugin_menu_show_configure) IA__xfce_panel_plugin_menu_show_configure __attribute__((visibility("hidden")));
#define xfce_panel_plugin_menu_show_configure IA__xfce_panel_plugin_menu_show_configure

extern __typeof(xfce_panel_plugin_menu_show_about) IA__xfce_panel_plugin_menu_show_about __attribute__((visibility("hidden")));
#define xfce_panel_plugin_menu_show_about IA__xfce_panel_plugin_menu_show_about

extern __typeof(xfce_panel_plugin_menu_destroy) IA__xfce_panel_plugin_menu_destroy __attribute__((visibility("hidden")));
#define xfce_panel_plugin_menu_destroy IA__xfce_panel_plugin_menu_destroy

extern __typeof(xfce_panel_plugin_block_menu) IA__xfce_panel_plugin_block_menu __attribute__((visibility("hidden")));
#define xfce_panel_plugin_block_menu IA__xfce_panel_plugin_block_menu

extern __typeof(xfce_panel_plugin_unblock_menu) IA__xfce_panel_plugin_unblock_menu __attribute__((visibility("hidden")));
#define xfce_panel_plugin_unblock_menu IA__xfce_panel_plugin_unblock_menu

extern __typeof(xfce_panel_plugin_register_menu) IA__xfce_panel_plugin_register_menu __attribute__((visibility("hidden")));
#define xfce_panel_plugin_register_menu IA__xfce_panel_plugin_register_menu

extern __typeof(xfce_panel_plugin_arrow_type) IA__xfce_panel_plugin_arrow_type __attribute__((visibility("hidden")));
#define xfce_panel_plugin_arrow_type IA__xfce_panel_plugin_arrow_type

extern __typeof(xfce_panel_plugin_remove) IA__xfce_panel_plugin_remove __attribute__((visibility("hidden")));
#define xfce_panel_plugin_remove IA__xfce_panel_plugin_remove

extern __typeof(xfce_panel_plugin_position_widget) IA__xfce_panel_plugin_position_widget __attribute__((visibility("hidden")));
#define xfce_panel_plugin_position_widget IA__xfce_panel_plugin_position_widget

extern __typeof(xfce_panel_plugin_position_menu) IA__xfce_panel_plugin_position_menu __attribute__((visibility("hidden")));
#define xfce_panel_plugin_position_menu IA__xfce_panel_plugin_position_menu

extern __typeof(xfce_panel_plugin_popup_menu) IA__xfce_panel_plugin_popup_menu __attribute__((visibility("hidden")));
#define xfce_panel_plugin_popup_menu IA__xfce_panel_plugin_popup_menu

extern __typeof(xfce_panel_plugin_popup_window) IA__xfce_panel_plugin_popup_window __attribute__((visibility("hidden")));
#define xfce_panel_plugin_popup_window IA__xfce_panel_plugin_popup_window

extern __typeof(xfce_panel_plugin_focus_widget) IA__xfce_panel_plugin_focus_widget __attribute__((visibility("hidden")));
#define xfce_panel_plugin_focus_widget IA__xfce_panel_plugin_focus_widget

extern __typeof(xfce_panel_plugin_block_autohide) IA__xfce_panel_plugin_block_autohide __attribute__((visibility("hidden")));
#define xfce_panel_plugin_block_autohide IA__xfce_panel_plugin_block_autohide

extern __typeof(xfce_panel_plugin_lookup_rc_file) IA__xfce_panel_plugin_lookup_rc_file __attribute__((visibility("hidden"))) G_GNUC_MALLOC G_GNUC_WARN_UNUSED_RESULT;
#define xfce_panel_plugin_lookup_rc_file IA__xfce_panel_plugin_lookup_rc_file

extern __typeof(xfce_panel_plugin_save_location) IA__xfce_panel_plugin_save_location __attribute__((visibility("hidden"))) G_GNUC_MALLOC G_GNUC_WARN_UNUSED_RESULT;
#define xfce_panel_plugin_save_location IA__xfce_panel_plugin_save_location

#endif /* __XFCE_PANEL_PLUGIN_H__ */


#ifdef __XFCE_PANEL_PLUGIN_PROVIDER_H__

extern __typeof(xfce_panel_plugin_provider_ask_remove) IA__xfce_panel_plugin_provider_ask_remove __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_ask_remove IA__xfce_panel_plugin_provider_ask_remove

extern __typeof(xfce_panel_plugin_provider_get_type) IA__xfce_panel_plugin_provider_get_type __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_get_type IA__xfce_panel_plugin_provider_get_type

extern __typeof(xfce_panel_plugin_provider_get_name) IA__xfce_panel_plugin_provider_get_name __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_get_name IA__xfce_panel_plugin_provider_get_name

extern __typeof(xfce_panel_plugin_provider_get_unique_id) IA__xfce_panel_plugin_provider_get_unique_id __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_get_unique_id IA__xfce_panel_plugin_provider_get_unique_id

extern __typeof(xfce_panel_plugin_provider_set_size) IA__xfce_panel_plugin_provider_set_size __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_set_size IA__xfce_panel_plugin_provider_set_size

extern __typeof(xfce_panel_plugin_provider_set_icon_size) IA__xfce_panel_plugin_provider_set_icon_size __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_set_icon_size IA__xfce_panel_plugin_provider_set_icon_size

extern __typeof(xfce_panel_plugin_provider_set_dark_mode) IA__xfce_panel_plugin_provider_set_dark_mode __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_set_dark_mode IA__xfce_panel_plugin_provider_set_dark_mode

extern __typeof(xfce_panel_plugin_provider_set_mode) IA__xfce_panel_plugin_provider_set_mode __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_set_mode IA__xfce_panel_plugin_provider_set_mode

extern __typeof(xfce_panel_plugin_provider_set_nrows) IA__xfce_panel_plugin_provider_set_nrows __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_set_nrows IA__xfce_panel_plugin_provider_set_nrows

extern __typeof(xfce_panel_plugin_provider_set_screen_position) IA__xfce_panel_plugin_provider_set_screen_position __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_set_screen_position IA__xfce_panel_plugin_provider_set_screen_position

extern __typeof(xfce_panel_plugin_provider_save) IA__xfce_panel_plugin_provider_save __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_save IA__xfce_panel_plugin_provider_save

extern __typeof(xfce_panel_plugin_provider_emit_hidden_event) IA__xfce_panel_plugin_provider_emit_hidden_event __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_emit_hidden_event IA__xfce_panel_plugin_provider_emit_hidden_event

extern __typeof(xfce_panel_plugin_provider_emit_signal) IA__xfce_panel_plugin_provider_emit_signal __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_emit_signal IA__xfce_panel_plugin_provider_emit_signal

extern __typeof(xfce_panel_plugin_provider_get_show_configure) IA__xfce_panel_plugin_provider_get_show_configure __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_get_show_configure IA__xfce_panel_plugin_provider_get_show_configure

extern __typeof(xfce_panel_plugin_provider_show_configure) IA__xfce_panel_plugin_provider_show_configure __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_show_configure IA__xfce_panel_plugin_provider_show_configure

extern __typeof(xfce_panel_plugin_provider_get_show_about) IA__xfce_panel_plugin_provider_get_show_about __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_get_show_about IA__xfce_panel_plugin_provider_get_show_about

extern __typeof(xfce_panel_plugin_provider_show_about) IA__xfce_panel_plugin_provider_show_about __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_show_about IA__xfce_panel_plugin_provider_show_about

extern __typeof(xfce_panel_plugin_provider_removed) IA__xfce_panel_plugin_provider_removed __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_removed IA__xfce_panel_plugin_provider_removed

extern __typeof(xfce_panel_plugin_provider_remote_event) IA__xfce_panel_plugin_provider_remote_event __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_remote_event IA__xfce_panel_plugin_provider_remote_event

extern __typeof(xfce_panel_plugin_provider_set_locked) IA__xfce_panel_plugin_provider_set_locked __attribute__((visibility("hidden")));
#define xfce_panel_plugin_provider_set_locked IA__xfce_panel_plugin_provider_set_locked

#endif /* __XFCE_PANEL_PLUGIN_PROVIDER_H__ */


G_GNUC_END_IGNORE_DEPRECATIONS

#endif /* ENABLE_SYMBOL_VISIBILITY && __GNUC__ && G_HAVE_GNUC_VISIBILITY */
